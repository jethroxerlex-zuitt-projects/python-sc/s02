# Python also allows input from users, with this, users can give inputs to the program

#[Section] input
# username = input("Please enter your name:\n");
# #\n = so we can type input in a new line
# print(f"Hello {username}! Welcome to python short course!");
# num1 = int(input("Enter first number: "));
# num2 = int(input("Enter second number: "));
# print(f"the sum of num1 and num2 is {num1+num2}");

#[Section] With the user inputs, users can give inputs for the program to be used to control the application using control structures.
# Control structures can be divided into selection and repitition control structures.

#Selection control structures allows the program to choose among choices and run specific codes depending on the choice taken(conditions)

#Repetition Control Structures allow the program to repeat certain blocks of code given a starting condition and terminating condition

#[Section] If-else statements
#if-else statement are used to choose between two or more code blocks depending on the condition

test_num = 75

if test_num >= 60 :
	print("Test passed!")
else:
	print("Test failed");

#In Python, curly braces "{}" are not needed to distinguish the code blocks inside the if or else block. Hence, indentations are important as python uses indentation to distinguish parts of code as needed.

#[Section] if else chains can also be used to have more than 2 choices for the program

# test_num2 = int(input("Please enter the second number\n"));

# if test_num2 > 0 :
# 	print("The number is positive!")
# 	print(f"The number you provided is {test_num2}!")
# elif test_num2 == 0 :
# 	print("The number is equal to 0!")
# else:
# 	print("The number is negative!")

# if test_num2%3 == 0 and test_num2%5 == 0 :
# 	print(f"{test_num2}is divisible by both 3 and 5" )
# elif test_num2%3 == 0 : 
# 	print(f"{test_num2} is divisible by 3")
# elif test_num2%5 == 0 :
# 	print(f"{test_num2} is divisible by 5")
# else:
# 	print(f"{test_num2} is not divisible by 3 nor 5")		

# test_num3 = int(input("Please enter a number to test: \n"))

# if test_num3%3 == 0 and test_num3 % 5 ==0:
# 	print("The number is divisible by 3 and 5!")
# elif test_num3 % 3 == 0	:
# 	print("The number is divisible by 3!")
# elif test_num3 % 5 == 0:
# 	print("The number is divisible by 5!")
# else:
# 	print("The number is not divisible by 3 or 5!")	

# [Section] Loops
# Python has loops that can repeat blocks of code
# While loops are used to execute a set of statements as long as the condition is true	

# i = 0

# while i <= 5 :
# 	print(f"Current value of i is {i}")	
# 	i += 1	

# [Section] for loops are used for iterating over a sequence

fruits = ["apple","banana","cherry"] #lists

for indiv_fruit in fruits:
	print(indiv_fruit);	

# [Section] range() method
# To use the for loopto iterate through values, the range method can be used

#default starting value is 0
# Syntax:
	# range(starting_value, limit, incrementation_value)
# for x in range(6):
# 	print(f"The current of x is {x}!")	

# for x in range(6, 10):
# 	print(f"The current of x is {x}!")	

for x in range(6, 20, 2):
 	print(f"The current of x is {x}!")	

# [Section] Break Statemnent
# The break statement is used to stop the loop

# j = 1 
# while j < 6 : 
# 	print(j)
	
# 	if j ==3:
# 		break;

# 	j+=1 	

# [Section] Continue Statement
# The continue statement it returns the control to the beginning of the while loop and continue to the next iteration

k = 1

while k < 6 :

	k+= 1 
	if k == 3:
		continue
	print(k)		