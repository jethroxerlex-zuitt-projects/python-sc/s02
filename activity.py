
year = int(input("Please enter what year it is: \n"));

if year <=0:
	print("You have inputed a invalid year");

elif year % 400 == 0 and year % 100 == 0:
	print(f"{year} is a leap year")
elif year % 4 == 0 and year % 100 != 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year");

row = int(input("Please enter number of rows: \n"))
col = int(input("Please enter number of columns: \n"))

for x in range(row):
    for y in range(col):
        print('*',end = ' ')
    print()			